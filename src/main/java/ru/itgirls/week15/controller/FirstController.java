package ru.itgirls.week15.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FirstController {
    enum Weekday{
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY,
        SUNDAY
    }

    @GetMapping("/dayOfWeek")
    public String dayOfWeek(@RequestParam(value = "weekday", defaultValue = "SUNDAY") String weekday) {
        Weekday today = Weekday.valueOf(weekday);
        switch (today) {
            case MONDAY -> weekday = "понедельник";
            case TUESDAY -> weekday = "вторник";
            case WEDNESDAY -> weekday = "среда";
            case THURSDAY -> weekday = "четверг";
            case FRIDAY -> weekday = "пятница";
            case SATURDAY -> weekday = "суббота";
            case SUNDAY -> weekday = "воскресенье";
        }
        return String.format("Сегодня %s", weekday);
    }
}
